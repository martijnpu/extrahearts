package me.martijnpu.extrahearts;

import me.martijnpu.extrahearts.cmds.CommandManager;
import me.martijnpu.extrahearts.data.ConfigData;
import me.martijnpu.extrahearts.data.FileHandler;
import me.martijnpu.extrahearts.data.MessageData;
import me.martijnpu.extrahearts.events.EventManager;
import me.martijnpu.extrahearts.events.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    public static boolean placeholderApiAvailable;
    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    public static boolean isPlaceholderApiAvailable() {
        return placeholderApiAvailable;
    }


    @Override
    public void onEnable() {
        instance = this;

        hookPlaceholderAPI();                           //Hook PlaceholderAPI

        EventManager.setupEvents();
        setup();

        Messages.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Done loading ExtraHearts =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n"
                + "                                                  We're up and running");
    }

    @Override
    public void onDisable() {
        shutdown();
        Messages.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Disabled successful ExtraHearts =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    }

    public boolean onReload() {
        try {
            Messages.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Start reloading ExtraHearts =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            shutdown();
            setup();
            Messages.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Done reloading ExtraHearts =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Messages.sendConsole("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Error while reloading ExtraHearts =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
            return false;
        }
    }

    private void setup() {
        ConfigData.getInstance().printInitData();
        PlayerDataManager.getInstance().printInitData();
        CommandManager.getInstance().printInitData();
        MessageData.printInitData();
    }

    private void shutdown() {
        ConfigData.resetInstance();
        PlayerDataManager.resetInstance();
        MessageData.reset();
        FileHandler.resetInstance();
    }

    private void hookPlaceholderAPI() {
        placeholderApiAvailable = Bukkit.getServer().getPluginManager().getPlugin("PlaceholderAPI") != null;
        if (placeholderApiAvailable) {
            Messages.sendConsole("Successfully hooked into PlaceholderAPI");
            new PlaceholderAPI().register();
        } else
            Messages.sendConsoleWarning("[ExtraHearts] No PlaceholderAPI dependency found. Disabling custom placeholders");
    }
}
