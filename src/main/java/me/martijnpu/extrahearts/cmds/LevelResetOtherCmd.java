package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import me.martijnpu.extrahearts.PlayerDataManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class LevelResetOtherCmd extends SubCommand {

    @Override
    String getCommand() {
        return "extrahearts";
    }

    @Override
    String getDescription() {
        return "Resets levels of a player";
    }

    @Override
    String getPermission() {
        return "extrahearts.admin";
    }

    @Override
    String[] getArgs() {
        return new String[]{"reset"};
    }

    @Override
    int getPlayerArguments() {
        return 1;
    }

    @Override
    String getUsage() {
        return super.getUsage() + " <player>";
    }

    @Override
    List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        return bigList;
    }

    @Override
    void onCommand(Player player, String[] args) {
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            Messages.sendMessage(player, Paths.MESS_CMD_OFFLINE);
            return;
        }
        PlayerDataManager.getInstance().resetAll(target);
        Messages.sendMessage(player, Paths.MESS_CMD_SUCCESS);
    }
}
