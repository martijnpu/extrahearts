package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Main;
import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import me.martijnpu.extrahearts.data.MessageData;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class CommandManager implements CommandExecutor {
    private static CommandManager instance;
    private final ArrayList<SubCommand> commands;

    private CommandManager() {
        TabComplete tabComplete = new TabComplete();

        Objects.requireNonNull(Main.getInstance().getCommand("extrahearts")).setExecutor(this);
        Objects.requireNonNull(Main.getInstance().getCommand("extrahearts")).setTabCompleter(tabComplete);

        commands = new ArrayList<>();
        commands.add(new AdminReloadCmd());
        commands.add(new HelpCmd());
        commands.add(new LevelAddOtherCmd());
        commands.add(new LevelInfoOtherCmd()); //Above other
        commands.add(new LevelInfoCmd());
        commands.add(new LevelRemoveOtherCmd());
        commands.add(new LevelResetOtherCmd());
    }

    public static CommandManager getInstance() {
        if (instance == null)
            instance = new CommandManager();
        return instance;
    }

    public void printInitData() {
        Messages.sendConsole("Registered " + commands.size() + " commands");
    }

    @Override
    public boolean onCommand(CommandSender s, Command command, String label,String[] args) {
        Player player = null;

        if (s instanceof Player)
            player = ((Player) s).getPlayer();

        loop:
        for (SubCommand subCommand : commands) {
            if (!subCommand.getCommand().equalsIgnoreCase(command.getName()))
                continue;

            if (args.length < subCommand.getArgs().length + subCommand.getPlayerArguments())
                continue;

            for (int i = 0; i < subCommand.getArgs().length; i++) {
                if (!subCommand.getArgs()[i].equalsIgnoreCase(args[i]))
                    continue loop;
            }

            if (!(s instanceof Player) && subCommand.playerOnly()) {
                Messages.sendConsole(Paths.MESS_CMD_PLAYER);
                return true;
            }

            if (!s.hasPermission(subCommand.getPermission()) && !subCommand.getPermission().isEmpty()) {
                Messages.sendMessage(player, Paths.MESS_CMD_NO_PERM);
                return true;
            }

            if (args.length < subCommand.getArgs().length + subCommand.getPlayerArguments()) {
                Messages.sendMessage(player, MessageData.getMessage(Paths.MESS_CMD_FORMAT).replace("%FORMAT%", subCommand.getUsage()));
                return true;
            }

            subCommand.onCommand(player, Arrays.copyOfRange(args, subCommand.getArgs().length, args.length));
            return true;
        }

        Messages.sendMessage(player, MessageData.getMessage(Paths.MESS_CMD_UNKNOWN).replace("%COMMAND%", label));
        return true;
    }

    ArrayList<SubCommand> getCommands() {
        return commands;
    }

    ArrayList<String> onHelpCommand(Player player, String commandName, int page) {
        int page_size = 15;
        ArrayList<String> list = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();
        page--;

        for (SubCommand command : commands) {
            if (!command.getCommand().equalsIgnoreCase(commandName) && !commandName.isEmpty())
                continue;

            if (player != null && !player.hasPermission(command.getPermission()) && !command.getPermission().isEmpty())
                continue;

            list.add(MessageData.getMessage(Paths.MESS_CMD_HELP_CMD).replace("%COMMAND%", command.getUsage()));
            list.add(MessageData.getMessage(Paths.MESS_CMD_HELP_DESC).replace("%DESCRIPTION%", command.getDescription()));
            list.add("");
        }

        int pages = list.size() / page_size;

        if (list.size() / page_size != 0)
            pages++;

        list2.add(MessageData.getMessage(Paths.MESS_CMD_HELP_PAGE).replace("%AMOUNT%", page + 1 + "").replace("%TOTAL%", pages + ""));

        int index = (page + 1) * page_size;

        if (((page + 1) * page_size) > list.size())
            index = list.size();

        if (page * page_size <= list.size())
            list2.addAll(list.subList(page * page_size, index));

        return list2;
    }
}
