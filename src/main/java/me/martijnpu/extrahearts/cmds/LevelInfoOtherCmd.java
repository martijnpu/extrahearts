package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.ConfigData;
import me.martijnpu.extrahearts.data.MessageData;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class LevelInfoOtherCmd extends SubCommand {

    @Override
    String getCommand() {
        return "extrahearts";
    }

    @Override
    String getDescription() {
        return "Show level info of a player";
    }

    @Override
    String getPermission() {
        return "extrahearts.admin";
    }

    @Override
    String[] getArgs() {
        return new String[]{"info"};
    }

    @Override
    int getPlayerArguments() {
        return 1;
    }

    @Override
    String getUsage() {
        return super.getUsage() + " <player>";
    }

    @Override
    List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        bigList.add(IntStream.rangeClosed(0, 9).mapToObj(i -> i + "").collect(Collectors.toList()));
        return bigList;
    }

    @Override
    void onCommand(Player player, String[] args) {
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            Messages.sendMessage(player, Paths.MESS_CMD_OFFLINE);
            return;
        }

        int kills = PlayerDataManager.getInstance().getKills(target);
        List<String> list = new ArrayList<>();
        list.add(MessageData.getMessage(Paths.MESS_HEART_INFO)
                .replaceAll("%AMOUNT%", PlayerDataManager.getInstance().getLevel(target) + "")
                .replaceAll("%MOBS%", kills + "")
                .replaceAll("%TOTAL%", ConfigData.getInstance().getKillsNeeded(kills) + ""));
        Messages.sendBigMessage(player, list);
    }
}
