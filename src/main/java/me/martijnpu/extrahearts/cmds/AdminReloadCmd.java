package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Main;
import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import org.bukkit.entity.Player;

class AdminReloadCmd extends SubCommand {

    @Override
    String getCommand() {
        return "extrahearts";
    }

    @Override
    String getDescription() {
        return "Reload the config and user files";
    }

    @Override
    String[] getArgs() {
        return new String[]{"reload"};
    }

    @Override
    String getPermission() {
        return "extrahearts.admin";
    }

    @Override
    void onCommand(Player player, String[] args) {
        if (Main.getInstance().onReload())
            Messages.sendMessage(player, Paths.MESS_CONFIG_SUCCESS);
        else
            Messages.sendMessage(player, Paths.MESS_CONFIG_ERROR);
    }
}
