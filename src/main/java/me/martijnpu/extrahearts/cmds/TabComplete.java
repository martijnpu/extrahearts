package me.martijnpu.extrahearts.cmds;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TabComplete implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> list = new ArrayList<>();

        loop:
        for (SubCommand command : CommandManager.getInstance().getCommands()) {
            if (!command.getCommand().equalsIgnoreCase(cmd.getName()))
                continue;

            if (args.length > command.getArgs().length + command.getPlayerArguments())
                continue;

            if (!sender.hasPermission(command.getPermission()) && !command.getPermission().isEmpty())
                continue;

            int i;
            for (i = 0; i < args.length - 1 && i < command.getArgs().length; i++) {
                if (!args[i].equalsIgnoreCase(command.getArgs()[i]))
                    continue loop;
            }

            if (args.length <= command.getArgs().length)
                list.add(command.getArgs()[i]);
            else if (command.getPlayerArguments() > 0
                    && command.getPlayerArguments() > args.length - command.getArgs().length - 1
                    && command.getTabComplete() != null)
                list.addAll(command.getTabComplete().get(args.length - command.getArgs().length - 1));
        }

        List<String> tabSuggest = new ArrayList<>();

        for (int i = list.size() - 1; i >= 0; i--) {
            if (list.get(i).toLowerCase().startsWith(args[args.length - 1].toLowerCase()))
                tabSuggest.add(list.get(i));
        }

        Collections.sort(tabSuggest);
        return tabSuggest;

    }
}
