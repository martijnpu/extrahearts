package me.martijnpu.extrahearts.cmds;


import org.bukkit.entity.Player;

import java.util.List;

abstract class SubCommand {

    abstract void onCommand(Player player, String[] args);

    abstract String getCommand();

    abstract String getDescription();

    String[] getArgs() {
        return new String[]{};
    }

    boolean playerOnly() {
        return false;
    }

    String getPermission() {
        return "";
    }

    int getPlayerArguments() {
        return 0;
    }

    List<List<String>> getTabComplete() {
        return null;
    }

    String getUsage() {
        StringBuilder sb = new StringBuilder("/");
        sb.append(getCommand());
        for (String arg : getArgs())
            sb.append(" ").append(arg);
        return sb.toString();
    }
}
