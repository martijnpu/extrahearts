package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.MessageData;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class LevelAddOtherCmd extends SubCommand {

    @Override
    String getCommand() {
        return "extrahearts";
    }

    @Override
    String getDescription() {
        return "Add levels to a player";
    }

    @Override
    String getPermission() {
        return "extrahearts.admin";
    }

    @Override
    String[] getArgs() {
        return new String[]{"add"};
    }

    @Override
    int getPlayerArguments() {
        return 2;
    }

    @Override
    String getUsage() {
        return super.getUsage() + " <player> <amount>";
    }

    @Override
    List<List<String>> getTabComplete() {
        List<List<String>> bigList = new ArrayList<>();
        bigList.add(Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList()));
        bigList.add(IntStream.rangeClosed(0, 9).mapToObj(i -> i + "").collect(Collectors.toList()));
        return bigList;
    }

    @Override
    void onCommand(Player player, String[] args) {
        try {
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                Messages.sendMessage(player, Paths.MESS_CMD_OFFLINE);
                return;
            }

            PlayerDataManager.getInstance().addLevels(target, Integer.parseInt(args[1]));
            Messages.sendMessage(player, Paths.MESS_CMD_SUCCESS);
        } catch (NumberFormatException ex) {
            Messages.sendMessage(player, MessageData.getMessage(Paths.MESS_CMD_NUMBER).replaceAll("%NUMBER%", args[1]));
        }
    }
}
