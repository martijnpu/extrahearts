package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.ConfigData;
import me.martijnpu.extrahearts.data.MessageData;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

class LevelInfoCmd extends SubCommand {

    @Override
    String getCommand() {
        return "extrahearts";
    }

    @Override
    String getDescription() {
        return "Show level info";
    }

    @Override
    String[] getArgs() {
        return new String[]{"info"};
    }

    @Override
    void onCommand(Player player, String[] args) {
        int kills = PlayerDataManager.getInstance().getKills(player);
        List<String> list = new ArrayList<>();
        int level = PlayerDataManager.getInstance().getLevel(player);
        list.add(MessageData.getMessage(Paths.MESS_HEART_INFO)
                .replaceAll("%AMOUNT%", level + "")
                .replaceAll("%MOBS%", kills + "")
                .replaceAll("%TOTAL%", ConfigData.getInstance().getKillsNeeded(level) + ""));
        Messages.sendBigMessage(player, list);
    }
}
