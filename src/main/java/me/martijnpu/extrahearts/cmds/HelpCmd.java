package me.martijnpu.extrahearts.cmds;

import me.martijnpu.extrahearts.Messages;
import me.martijnpu.extrahearts.Paths;
import me.martijnpu.extrahearts.data.MessageData;
import org.bukkit.entity.Player;

class HelpCmd extends SubCommand {

    @Override
    String getCommand() {
        return "extrahearts";
    }

    @Override
    String getDescription() {
        return "Shows all the available commands";
    }

    @Override
    String[] getArgs() {
        return new String[]{"help"};
    }

    @Override
    void onCommand(Player player, String[] args) {
        try {
            if (args.length == 0)
                Messages.sendBigMessage(player, CommandManager.getInstance().onHelpCommand(player, getCommand(), 1));
            else
                Messages.sendBigMessage(player, CommandManager.getInstance().onHelpCommand(player, getCommand(), Integer.parseInt(args[0])));
        } catch (Exception ex) {
            Messages.sendMessage(player, MessageData.getMessage(Paths.MESS_CMD_NUMBER).replaceAll("%NUMBER%", args[0]));
        }
    }
}
