package me.martijnpu.extrahearts.events;

import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.ConfigData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
            PlayerDataManager.getInstance().setHealth(event.getPlayer());
    }
}

