package me.martijnpu.extrahearts.events;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.martijnpu.extrahearts.Main;
import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.ConfigData;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

/**
 * This class will automatically register as a placeholder expansion
 * when a jar including this class is added to the directory
 * {@code /plugins/PlaceholderAPI/expansions} on your server.
 * <p>
 * If you create such a class inside your own plugin, you have to
 * register it manually in your plugins {@code onEnable()} by using
 * {@code new YourExpansionClass().register();}
 */
public class PlaceholderAPI extends PlaceholderExpansion {
    /**
     * Because this is an internal class,
     * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
     * PlaceholderAPI is reloaded
     *
     * @return true to persist through reloads
     */
    @Override
    public boolean persist() {
        return true;
    }

    /**
     * Because this is a internal class, this check is not needed
     * and we can simply return {@code true}
     *
     * @return Always true since it's an internal class.
     */
    @Override
    public boolean canRegister() {
        return true;
    }

    /**
     * The name of the person who created this expansion should go here.
     * <br>For convenience do we return the author from the plugin.yml
     *
     * @return The name of the author as a String.
     */
    @Override
    public @NotNull String getAuthor() {
        return Main.getInstance().getDescription().getAuthors().toString();
    }

    /**
     * The placeholder identifier should go here.
     * <br>This is what tells PlaceholderAPI to call our onRequest
     * method to obtain a value if a placeholder starts with our
     * identifier.
     * <br>This must be unique and can not contain % or _
     *
     * @return The identifier in {@code %<identifier>_<value>%} as String.
     */
    @Override
    public @NotNull String getIdentifier() {
        return "extrahearts";
    }

    /**
     * This is the version of the expansion.
     * <br>You don't have to use numbers, since it is set as a String.
     * <p>
     * For convenience do we return the version from the plugin.yml
     *
     * @return The version as a String.
     */
    @Override
    public @NotNull String getVersion() {
        return Main.getInstance().getDescription().getVersion();
    }

    /**
     * This is the method called when a placeholder with our identifier
     * is found and needs a value.
     * <br>We specify the value identifier in this method.
     * <br>Since version 2.9.1 can you use OfflinePlayers in your requests.
     *
     * @param player     A {@link Player Player}.
     * @param identifier A String containing the identifier/value.
     * @return possibly-null String of the requested identifier.
     */
    @Override
    public String onPlaceholderRequest(Player player, @NotNull String identifier) {
        if (player == null)
            return "";

        String[] args = identifier.split("_");

        switch (args[0]) {
            // %extrahearts_hearts%
            case "hearts":
                return PlayerDataManager.getInstance().getLevel(player) + "";

            // %extrahearts_kills%
            case "kills":
                return PlayerDataManager.getInstance().getKills(player) + "";

            // %extrahearts_total%
            case "total":
                return ConfigData.getInstance().getKillsNeeded(PlayerDataManager.getInstance().getLevel(player)) + "";

            // %extrahearts_needed%
            case "needed":
                return ConfigData.getInstance().getKillsNeeded(PlayerDataManager.getInstance().getLevel(player)) - PlayerDataManager.getInstance().getKills(player) + "";

            // We return null if an invalid placeholder (f.e. %someplugin_placeholder3%) was provided
            default:
                return null;
        }
    }
}
