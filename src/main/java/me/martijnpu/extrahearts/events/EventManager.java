package me.martijnpu.extrahearts.events;

import me.martijnpu.extrahearts.Main;
import org.bukkit.Bukkit;

public class EventManager {

    public static void setupEvents() {
        Bukkit.getServer().getPluginManager().registerEvents(new EntityDeath(), Main.getInstance());
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerRespawn(), Main.getInstance());
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(), Main.getInstance());
    }
}
