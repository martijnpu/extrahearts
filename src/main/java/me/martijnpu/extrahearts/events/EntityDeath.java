package me.martijnpu.extrahearts.events;

import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.ConfigData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EntityDeath implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onMobKill(EntityDeathEvent event) {
        if (ConfigData.getInstance().isDefinedMob(event.getEntityType()))
            PlayerDataManager.getInstance().addMobKill(event.getEntity().getKiller());
    }
}

