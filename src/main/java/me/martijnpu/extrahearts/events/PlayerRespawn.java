package me.martijnpu.extrahearts.events;

import me.martijnpu.extrahearts.PlayerDataManager;
import me.martijnpu.extrahearts.data.ConfigData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawn implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerRespawnEvent e) {
        PlayerDataManager.getInstance().removeLevels(e.getPlayer(), ConfigData.getInstance().removeLevelOnDeath());
    }
}
