package me.martijnpu.extrahearts;

import me.martijnpu.extrahearts.data.MessageData;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class Messages {
    public static final boolean DEBUG = true;

    public static void sendMessage(Player player, String message) {
        if (message.isEmpty()) return;
        if (player == null)
            sendConsole(colorMessage(message));
        else {
            try {
                sendJSON(player, message);
            } catch (Exception ex) {
                player.sendMessage(colorMessage(message));
            }
        }
    }

    public static void sendBigMessage(Player player, List<String> messageLines) {
        if (player == null)
            sendConsole(colorMessageHeadFooter(messageLines));
        else
            player.sendMessage(colorMessageHeadFooter(messageLines));
    }

    public static void sendConsole(String message) {
        System.out.println("[" + Main.getInstance().getDescription().getName() + "] " + format(message));
    }

    public static void sendConsoleWarning(String message) {
        Main.getInstance().getLogger().warning("[" + Main.getInstance().getDescription().getName() + "] " + format(message));
    }

    public static void sendConsoleDebug(String message) {
        if (DEBUG)
            System.out.println("[" + Main.getInstance().getDescription().getFullName() + "] DEBUG " + format(message));
    }

    public static String format(String message) {
        if (message == null || message.isEmpty()) return "";
        String messageData = MessageData.getMessage(message);
        return ChatColor.translateAlternateColorCodes('&', messageData);
    }

    private static String colorMessage(String message) {
        if (message.isEmpty()) return "";
        return format(MessageData.getPrefix()) + format(message);
    }

    private static void sendJSON(Player player, String message) {
        if (message.isEmpty())
            return;
        TextComponent totalMessage = new TextComponent();
        for (BaseComponent baseComponent : TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', MessageData.getPrefix()))) {
            totalMessage.addExtra(baseComponent);
        }
        String messageData = MessageData.getMessage(message);
        if (messageData == null || messageData.isEmpty())
            messageData = message;

        for (BaseComponent baseComponent : ComponentSerializer.parse(messageData)) {
            totalMessage.addExtra(baseComponent);
            if (baseComponent.toLegacyText().contains("\n")) {
                for (BaseComponent baseComponentPrefix : TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', MessageData.getPrefix()))) {
                    totalMessage.addExtra(baseComponentPrefix);
                }
            }
        }
        player.spigot().sendMessage(totalMessage);
    }

    private static String colorMessageHeadFooter(List<String> messageLine) {
        if (messageLine.isEmpty()) return "";
        StringBuilder sb = new StringBuilder("\n" + format(Paths.MESS_HEADER) + "\n");
        messageLine.forEach(x -> sb.append(format(x)).append("\n"));
        sb.append(format(Paths.MESS_FOOTER)).append("\n");
        return sb.toString();
    }
}
