package me.martijnpu.extrahearts;

import me.martijnpu.extrahearts.data.ConfigData;
import me.martijnpu.extrahearts.data.FileHandler;
import me.martijnpu.extrahearts.data.MessageData;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class PlayerDataManager {
    private static final String PATH = "user";
    private static PlayerDataManager instance;

    private PlayerDataManager() {
    }

    public static PlayerDataManager getInstance() {
        if (instance == null)
            instance = new PlayerDataManager();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public void printInitData() {
        ConfigurationSection cs = FileHandler.getInstance().getUserdataConfig().getConfigurationSection(PATH);
        int players = 0;
        if (cs != null)
            players = cs.getKeys(false).size();
        Messages.sendConsole("Loading " + players + " users.");
    }

    public void addMobKill(Player player) {
        if (player == null)
            return;

        boolean leveled = false;
        int level = getLevel(player);
        int kills = getKills(player);
        kills++;

        while (kills >= ConfigData.getInstance().getKillsNeeded(level)) {
            kills -= ConfigData.getInstance().getKillsNeeded(level);
            addLevels(player, 1);
            level = getLevel(player);
            leveled = true;
        }
        FileHandler.getInstance().getUserdataConfig().set(PATH + "." + player.getUniqueId() + ".kills", kills);
        FileHandler.getInstance().saveUsersConfig();

        if (!leveled)
            Messages.sendMessage(player, MessageData.getMessage(Paths.MESS_HEART_KILL).replaceAll("%AMOUNT%", ConfigData.getInstance().getKillsNeeded(level) - kills + ""));
    }

    /**
     * @param player The player
     * @return Difference of health set
     */
    public double setHealth(Player player) {
        double currHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        double addHealth = ConfigData.getInstance().getHeartAddAmount() * getLevel(player);
        double addHealthCap = Math.min(addHealth, ConfigData.getInstance().getMaxExtraHearts());
        double newHealth = Math.min(addHealthCap + 20, 2048); //Cap Max value
        if(addHealthCap == ConfigData.getInstance().getMaxExtraHearts())
            return 9999;

        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(newHealth);
        player.setHealth(Math.min(currHealth + addHealthCap, newHealth));
        return newHealth - currHealth;
    }

    public void addLevels(Player player, int levels) {
        int currLevel = getLevel(player);

        FileHandler.getInstance().getUserdataConfig().set(PATH + "." + player.getUniqueId() + ".level", currLevel + levels);
        FileHandler.getInstance().saveUsersConfig();

        double extraHealth = setHealth(player);
        if (extraHealth == 9999)
            Messages.sendMessage(player, Paths.MESS_HEART_MAX);
        else {
            String title = Messages.format(Paths.MESS_HEART_ADD_TITLE);
            String subtitle = Messages.format(Paths.MESS_HEART_ADD_SUBTITLE)
                    .replace("%NAME%", player.getName())
                    .replace("%AMOUNT%", extraHealth * 0.5 + "");
            if(ConfigData.getInstance().getNotifyEveryoneOnLevelUp())
                Bukkit.getOnlinePlayers().forEach(x -> x.sendTitle(title, subtitle, 10, 100, 10));
            else player.sendTitle(title, subtitle, 10, 100, 10);
        }
    }

    public void removeLevels(Player player, int levels) {
        if (levels < 0)
            resetAll(player);
        if (levels <= 0)
            return;

        FileHandler.getInstance().getUserdataConfig().set(PATH + "." + player.getUniqueId() + ".level", Math.max(getLevel(player) - levels, 0));
        FileHandler.getInstance().getUserdataConfig().set(PATH + "." + player.getUniqueId() + ".kills", 0);
        FileHandler.getInstance().saveUsersConfig();

        double extraHealth = -setHealth(player);
        if (extraHealth > 0 && extraHealth != 9999)
            Messages.sendMessage(player, MessageData.getMessage(Paths.MESS_HEART_REMOVE).replaceAll("%AMOUNT%", extraHealth * 0.5 + ""));
    }

    public int getKills(Player player) {
        return FileHandler.getInstance().getUserdataConfig().getInt(PATH + "." + player.getUniqueId() + ".kills", 0);
    }

    public int getLevel(Player player) {
        return FileHandler.getInstance().getUserdataConfig().getInt(PATH + "." + player.getUniqueId() + ".level", 0);
    }

    public void resetAll(Player player) {
        FileHandler.getInstance().getUserdataConfig().set(PATH + "." + player.getUniqueId(), null);
        FileHandler.getInstance().saveUsersConfig();
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20.0);
        Messages.sendMessage(player, Paths.MESS_HEART_RESET);
    }
}
