package me.martijnpu.extrahearts;

public class Paths {
    public static final String MESS_HEADER = "header";
    public static final String MESS_FOOTER = "footer";

    public static final String MESS_CMD_FORMAT = "command.format";
    public static final String MESS_CMD_NO_PERM = "command.no-permission";
    public static final String MESS_CMD_PLAYER = "command.player-only";
    public static final String MESS_CMD_SUCCESS = "command.succeeded";
    public static final String MESS_CMD_UNKNOWN = "command.unknown";
    public static final String MESS_CMD_OFFLINE = "command.offline";
    public static final String MESS_CMD_NUMBER = "command.number";
    public static final String MESS_CMD_HELP_CMD = "help.command";
    public static final String MESS_CMD_HELP_DESC = "help.description";
    public static final String MESS_CMD_HELP_PAGE = "help.page";

    public static final String MESS_CONFIG_ERROR = "config.error";
    public static final String MESS_CONFIG_SUCCESS = "config.success";

    public static final String MESS_HEART_ADD_TITLE =   "heart.add-title.title";
    public static final String MESS_HEART_ADD_SUBTITLE =   "heart.add-title.subtitle";
    public static final String MESS_HEART_REMOVE = "heart.remove";
    public static final String MESS_HEART_INFO =   "heart.info";
    public static final String MESS_HEART_MAX =   "heart.max";
    public static final String MESS_HEART_KILL =   "heart.kill";
    public static final String MESS_HEART_RESET =   "heart.reset";

}
