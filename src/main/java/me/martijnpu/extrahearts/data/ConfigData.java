package me.martijnpu.extrahearts.data;

import me.martijnpu.extrahearts.Main;
import me.martijnpu.extrahearts.Messages;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

import java.util.*;

import static me.martijnpu.extrahearts.data.FileHandler.setNotExists;

public class ConfigData {
    private static ConfigData instance;
    private final TreeMap<Integer, Integer> killsNeeded;
    private final List<EntityType> mobTypes;

    //config
    private final String LEVEL_HEART_PATH = "amount-heart-per-level";
    private final String REMOVE_LEVEL_DEATH_PATH = "remove-level-on-playerdeath";
    private final String MAX_HEARTS_PATH = "max-extra-hearts";
    private final String NOTIFY_EVERYONE_PATH = "notify-everyone-when-level-up";
    private final String MOB_TYPES_PATH = "mob-types";
    private final String KILL_NEEDED_PATH = "mob-kills-needed";

    private ConfigData() {
        killsNeeded = new TreeMap<>();
        mobTypes = new ArrayList<>();

        boolean saveConfig;
        Main.getInstance().saveDefaultConfig();
        Main.getInstance().reloadConfig();

        saveConfig = setNotExists(Main.getInstance().getConfig(), LEVEL_HEART_PATH, 2);
        saveConfig |= setNotExists(Main.getInstance().getConfig(), REMOVE_LEVEL_DEATH_PATH, 1);
        saveConfig |= setNotExists(Main.getInstance().getConfig(), MAX_HEARTS_PATH, 60);
        saveConfig |= setNotExists(Main.getInstance().getConfig(), NOTIFY_EVERYONE_PATH, true);
        saveConfig |= setNotExists(Main.getInstance().getConfig(), MOB_TYPES_PATH, new String[]{"ZOMBIE", "SKELETON"});
        saveConfig |= setNotExists(Main.getInstance().getConfig(), KILL_NEEDED_PATH, new HashMap<Integer, Integer>() {{
            put(1, 100);
            put(2, 120);
        }});

        if (saveConfig)
            Main.getInstance().saveConfig();

        readMobKills();
        readMobTypes();
    }

    public static ConfigData getInstance() {
        if (instance == null)
            instance = new ConfigData();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    private void readMobKills() {
        ConfigurationSection cs = Main.getInstance().getConfig().getConfigurationSection(KILL_NEEDED_PATH);
        if (cs != null)
            for (String key : cs.getKeys(false)) {
                try {
                    killsNeeded.put(Integer.valueOf(key), cs.getInt(key));
                } catch (NumberFormatException ex) {
                    Messages.sendConsoleWarning("Level '" + key + "' is an invalid !");
                }
            }
    }

    private void readMobTypes() {
        List<String> mobs = Main.getInstance().getConfig().getStringList(MOB_TYPES_PATH);
        for (String mob : mobs) {
            try {
                mobTypes.add(EntityType.valueOf(mob.toUpperCase()));
            } catch (IllegalArgumentException ex) {
                Messages.sendConsoleWarning("MobType '" + mob + "' is not an valid EntityType!");
            }
        }
    }

    public void printInitData() {
        Messages.sendConsole("Loaded config");
        Messages.sendConsole("Loaded " + killsNeeded.size() + " level-kills");
        Messages.sendConsole("Loaded " + mobTypes.size() + " mobTypes");
    }

    //region getters en setters

    public int getHeartAddAmount() {
        return Main.getInstance().getConfig().getInt(LEVEL_HEART_PATH, 2);
    }

    public int removeLevelOnDeath() {
        return Main.getInstance().getConfig().getInt(REMOVE_LEVEL_DEATH_PATH, 1);
    }

    public int getMaxExtraHearts() {
        return Main.getInstance().getConfig().getInt(MAX_HEARTS_PATH, 60);
    }

    public boolean getNotifyEveryoneOnLevelUp() {
        return Main.getInstance().getConfig().getBoolean(NOTIFY_EVERYONE_PATH, true);
    }

    public int getKillsNeeded(int currentLevel) {
        Map.Entry<Integer, Integer> kills = killsNeeded.floorEntry(currentLevel + 1);
        return (kills == null ? 10 : kills.getValue());
    }

    public boolean isDefinedMob(EntityType entityType) {
        return mobTypes.contains(entityType);
    }

    //endregion getters en setters
}
