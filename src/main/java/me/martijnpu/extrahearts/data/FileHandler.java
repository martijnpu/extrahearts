package me.martijnpu.extrahearts.data;

import me.martijnpu.extrahearts.Main;
import me.martijnpu.extrahearts.Messages;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class FileHandler {
    private static FileHandler instance;
    private final File messagesFile = new File(Main.getInstance().getDataFolder(), "/messages.yml");
    private final File userdataFile = new File(Main.getInstance().getDataFolder(), "/userdata.yml");
    private FileConfiguration messagesConfig = null;
    private FileConfiguration userdataConfig = null;

    private FileHandler() {
    }

    public static FileHandler getInstance() {
        if (instance == null)
            instance = new FileHandler();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public static boolean setNotExists(FileConfiguration config, String path, Object defaultValue) {
        if (config.isSet(path))
            return false;
        Messages.sendConsoleWarning("Missing data at '" + path + "'. Setting the default now...");
        config.set(path, defaultValue);
        return true;
    }

    public FileConfiguration getMessagesConfig() {
        if (messagesConfig == null)
            messagesConfig = loadConfig(messagesFile);
        return messagesConfig;
    }

    public FileConfiguration getUserdataConfig() {
        if (userdataConfig == null)
            userdataConfig = loadConfig(userdataFile);
        return userdataConfig;
    }

    public void saveUsersConfig() {
        saveConfig(userdataConfig, userdataFile);
    }

    private FileConfiguration loadConfig(File file) {
        if (!file.exists())
            Main.getInstance().saveResource(file.getName(), false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        if (config.getKeys(false).size() == 0)
            config = backupConfig(file);
        return config;
    }

    private FileConfiguration backupConfig(File originFile) {
        String backupName = "backups/" + originFile.getName().replace(".yml", "") + "_backup";
        File backupFile = new File(Main.getInstance().getDataFolder(), "backups");
        if (!backupFile.exists() && !backupFile.mkdirs())
            Messages.sendConsole("Failed to create backup directory");
        int i = 1;
        while (backupFile.exists())
            backupFile = new File(Main.getInstance().getDataFolder(), backupName + "_" + i++ + ".yml");
        Messages.sendConsole("Renaming to " + backupFile.getName());
        if (!originFile.renameTo(backupFile))
            Messages.sendConsole("Failed creating backup");
        originFile.delete();
        Main.getInstance().saveResource(originFile.getName(), true);
        return YamlConfiguration.loadConfiguration(originFile);
    }

    private void saveConfig(FileConfiguration config, File file) {
        if (config == null)
            return;
        try {
            config.save(file);
        } catch (IOException ex) {
            Main.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + file, ex);
        }
    }
}
