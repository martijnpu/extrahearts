package me.martijnpu.extrahearts.data;

import me.martijnpu.extrahearts.Messages;

import java.util.List;

public class MessageData {
    private static String prefix;

    static {
        reset();
    }

    public static void reset() {
        prefix = FileHandler.getInstance().getMessagesConfig().getString("prefix", "ExtraHearts");
    }

    public static void printInitData() {
        Messages.sendConsole("Loading " + FileHandler.getInstance().getMessagesConfig().getKeys(true).size() + " messages");
    }

    public static String getPrefix() {
        return prefix;
    }

    public static String getMessage(String path) {
        return FileHandler.getInstance().getMessagesConfig().getString("messages." + path, path);
    }

    public static List<String> getMessageList(String path) {
        return FileHandler.getInstance().getMessagesConfig().getStringList("messages." + path);
    }
}
